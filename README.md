## Notice
A small utility written in Go to send messages to notifications-daemon.  
I realize there is already `notify-send` on most (if not all) distros  
but for some reason it wasn't work for me and I didn't feel like
trouble-shooting it, so I just wrote my own utility.  

This tool makes use of mqu's [go-notify](http://www.github.com/mqu/go-notify)
which made the utility super easy and super fast to make.

### Usage

``` sh
notice -m "The message you want to send" -i "path to image to attach to notification"
```
If an image path is not specified, the notification will display a generic
"info" icon.  If a message is not supplied, then what could you possibly need to
send create a notification about? Quotes are needed for it to work correctly
since the shell has a natural whitespace delimiter.
