package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os/user"
	"strings"
	"time"

	"github.com/mqu/go-notify"
)

func main() {
	msg := flag.String("m", "", "The message to send to notifications daemon")
	img := flag.String("i", "dialog-information", "The path of image to attach to notification")

	flag.Parse()

	rand.Seed(time.Now().Unix())
	randTitle := []string{"So, um...", "Hey!", "I forgot to mention...", "Dude, lemme tell you", "This just in!"}
	randIdx := rand.Intn(len(randTitle))

	usr, err := user.Current()
	if err != nil {
		fmt.Errorf("Error: %v", err)
	}

	imgStr := *img
	if string(imgStr[0]) == "~" {
		var sb strings.Builder
		sb.WriteString(usr.HomeDir + "/" + imgStr[2:])
		imgStr = sb.String()
	}

	notify.Init("Notice")
	n := notify.NotificationNew(randTitle[randIdx], *msg, imgStr)
	if err := n.Show(); err != nil {
		fmt.Errorf("Error: %s", err.Message())
	}
}
